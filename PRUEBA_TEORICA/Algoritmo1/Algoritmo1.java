import java.util.Arrays;

/**
 * Algoritmo1
 */
public class Algoritmo1 {
    public static void main(String[] args) {
        // int[] myArray = {  1, 2, 2, 5, 4, 6, 7, 8, 8, 8 };
        int[] myArray = {4,8,9,5,4,7,8,8,9,9,9,9};

        // Ordenar el arreglo de menor a mayor
        Arrays.sort(myArray);

        //Inicializar las variables
        int cuentaActual = 1;
        int cuentaMax = 0;
        int masFrecuenteNum = myArray[0];

        //Bucles
        for (int i = 1; i < myArray.length; i++) {
            if (myArray[i] == myArray[i - 1]) {
                cuentaActual++;
            } else {
                if (cuentaActual > cuentaMax) {
                    cuentaMax = cuentaActual;
                    masFrecuenteNum = myArray[i - 1];
                }
                cuentaActual = 1;
            }
        }

        // Verifica si el último número tiene la ocurrencia más larga
        if (cuentaActual > cuentaMax) {
            cuentaMax = cuentaActual;
            masFrecuenteNum = myArray[myArray.length - 1];
        } else if (cuentaActual == cuentaMax && myArray[myArray.length - 1] != masFrecuenteNum) {
            masFrecuenteNum = myArray[myArray.length - 1];
        }

        //Imprime
        System.out.println("Recurrencias: " + cuentaMax);
        System.out.println("Número: " + masFrecuenteNum);
    }
}