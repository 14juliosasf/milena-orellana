public class Algoritmo2 {
    public static void main(String[] args) {

        // Area de 4x4
        int [][] area = new int [4][4]; 
        int[] myArray = { 1, 2, -1, 1, 0, 1, 2, -1, -1, -2 };
        int x = 0;
        int y = 0;

        for (int i = 0; i < myArray.length; i += 2) {
            int moveX = myArray[i];
            int moveY = myArray[i + 1];

            // Actualizar la posición de la X
            x += moveX;
            y += moveY;

            // Ajustar la posición para que la X permanezca dentro del área
            x = Math.max(0, Math.min(x, 3));
            y = Math.max(0, Math.min(y, 3));

            // Marcar la posición de la X en el área
            area[y][x] = 1;
        }

        // Imprimir el área con la posición final de la X
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                if (i == y && j == x) {
                    System.out.print("X");
                } else {
                    System.out.print("O");
                }
            }
            System.out.println();
        }
    }
}
